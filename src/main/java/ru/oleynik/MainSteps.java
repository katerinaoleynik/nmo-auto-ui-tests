package ru.oleynik;

import com.codeborne.selenide.Condition;
import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Тогда;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.junit.Assert.assertTrue;

public class MainSteps {
    @Дано("^ПОльзователь открывает главную$")
    public void пользовательОткрываетГлавную() throws Throwable {
        open("http://1143.ru:3005/");
    }

    @Тогда("^Отображается текст \"([^\"]*)\"$")
    public void отображаетсяТекст(String arg1) throws Throwable {
        assertTrue($(byText(arg1)).waitUntil(Condition.visible, 20000).exists());
    }

    @Дано("^Пользователь нажимает на кнопку \"([^\"]*)\"$")
    public void пользовательНажимаетНаКнопку(String arg1) throws Throwable {
        $(byText(arg1)).waitUntil(Condition.visible, 20000).click();

    }

}
